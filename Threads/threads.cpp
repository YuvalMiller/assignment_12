#include "threads.h"

#define BLUE "Color 01"
#define GREEN "Color 02"
#define AQUA "Color 03"
#define RED "Color 04"
#define PURPLE "Color 05"
#define YELLOW "Color 06"
#define WHITE "Color 07"
#define GRAY "Color 08"
#define LIGHT_BLUE "Color 09"
#define LIGHT_GREEN "Color 0A"
#define LIGHT_AQUA "Color 0B"
#define LIGHT_RED "Color 0C"
#define LIGHT_PURPLE "Color 0D"
#define LIGHT_YELLOW "Color 0E"
#define BRIGHT_WHITE "Color 0F"

#define COLOR_PRINT 10

#define RANGE1 100
#define RANGE2 200
#define RANGE3 300
#define RANGE4 400
#define RANGE5 500
#define RANGE6 600
#define RANGE7 700
#define RANGE8 800
#define RANGE9 900
#define RANGE10 1000

#define PRIME_NUM 2

#define NUM_OF_THREADS 50

std::mutex mu;

/*
 * function prints "i love threads". thats it
 * return - none
 */
void I_Love_Threads()
{
	std::cout << "I love threads" << std::endl;
}

/*
 * function created a thread that calls I_Love_Threads() and closes it
 * return - none
 */
void call_I_Love_Threads()
{
	std::thread loveThread(I_Love_Threads);//creating the thread

	loveThread.join();//closing it
}

/*
 * fucntion gets an int vector and prints its values
 * param primes - the vector
 */
void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); ++i)
	{
		std::cout << primes[i] << std::endl;//printing the value
	}
}

/*
 * function gets a range of numbers, adds the primary numbers in the range to the vector
 * param begin - the start of the range
 * param end - the end of the range
 * param primes - the vector to add the primary numbers to
 * return - none
 */
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	for (int i = begin; i <= end ; ++i)
	{
		if (isPrime(i))//checking if the numbers is primary
		{
			primes.push_back(i);
		}
	}
}

/*
 * function creates a thread that calls getPrimes() and returns the vector
 * param begin - the start of the range of getPrimes()
 * param end - the end of the range for getPrimes();
 */
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;

	auto start = std::chrono::high_resolution_clock::now();//starting to measure time
	
	std::thread primeThread(getPrimes, begin, end, std::ref(primes));//creating the thread and calling the func

	primeThread.join();//stopping the thread

	auto finish = std::chrono::high_resolution_clock::now();//stopping measuring time
	std::chrono::duration<double> elapsed = finish - start;//calculating the elapsed time
	std::cout << elapsed.count() << std::endl;//printing it
	
	return primes;
}

/*
 * function gets a range and writes to a file the primary nums in the range
 * param begin - the start of the range
 * param end - the end of the range
 * param file - the file to write to
 */
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	for (int i = begin; i <= end; ++i)
	{
		if (isPrime(i))//checkin if primary
		{
			std::lock_guard<std::mutex> locker(mu);//added the lock_guard
			file << i << std::endl;//writing to file
		}
	}
}

/*
 * function creates N threads and calls writePrimesToFile() N times for N small ranges so it goes super fast
 * param begin - the start of the range
 * param end - the end of the range
 * param filePath - the path of the file
 * param N - the magic num
 */
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int diff = end - begin;
	diff /= N;//calculating N in the range

	std::ofstream f;
	f.open(filePath);//creating the file

	auto start = std::chrono::high_resolution_clock::now();//starting to measure time

	for (int i = begin; i < end - diff; i += diff)
	{
		std::thread currThread(writePrimesToFile, i, i + diff, std::ref(f));//creating the thread and calling the func

		currThread.join();//closing the thread
	}

	auto finish = std::chrono::high_resolution_clock::now();//stopping measuring time
	std::chrono::duration<double> elapsed = finish - start;//calculating the elapsed time
	std::cout << elapsed.count() << std::endl;//printing it
}

/*
 * function checnges the color of the console bcs i found no better way to do it, then prints "Color" 10 times
 * param color - the color to the system
 * return - none
 */
void printColor(const char* color)
{
	system(color);//changing the color of the console
	std::cout << std::endl;
	for (int i = 0; i < COLOR_PRINT; ++i)
	{
		std::cout << "Color" << std::endl;//printing
	}
}

/*
 * function creates 15 threads and calls printColor() with 15 colors
 */
void callPrintColor()
{
	std::thread t1(printColor, BLUE);//creating the threads and calling printColor
	system(("pause"));
	std::cout << std::endl;
	std::thread t2(printColor, GREEN);
	system(("pause"));
	std::cout << std::endl;
	std::thread t3(printColor, AQUA);
	system(("pause"));
	std::cout << std::endl;
	std::thread t4(printColor, RED);
	system(("pause"));
	std::cout << std::endl;
	std::thread t5(printColor, PURPLE);
	system(("pause"));
	std::cout << std::endl;
	std::thread t6(printColor, YELLOW);
	system(("pause"));
	std::cout << std::endl;
	std::thread t7(printColor, WHITE);
	system(("pause"));
	std::cout << std::endl;
	std::thread t8(printColor, GRAY);
	system(("pause"));
	std::cout << std::endl;
	std::thread t9(printColor, LIGHT_BLUE);
	system(("pause"));
	std::cout << std::endl;
	std::thread t10(printColor, LIGHT_GREEN);
	system(("pause"));
	std::cout << std::endl;
	std::thread t11(printColor, LIGHT_AQUA);
	system(("pause"));
	std::cout << std::endl;
	std::thread t12(printColor, LIGHT_RED);
	system(("pause"));
	std::cout << std::endl;
	std::thread t13(printColor, LIGHT_PURPLE);
	system(("pause"));
	std::cout << std::endl;
	std::thread t14(printColor, LIGHT_YELLOW);
	system(("pause"));
	std::cout << std::endl;
	std::thread t15(printColor, BRIGHT_WHITE);

	t1.join();//closing all the threads
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();
	t10.join();
	t11.join();
	t12.join();
	t13.join();
	t14.join();
	t15.join();
}

/*
 * function creates a 1000 lengthed randomized array, returns the max value in it - checks it with threads
 * return - the max num in the created array
 */
int findMax()
{
	srand(time(NULL));//idk

	int max = 0;
	int arr[1000] = {rand() % 1000000 + 1};

	std::thread t1(findMaxRange, 0, RANGE1, arr, &max);//creeating the threads, calling the function
	std::thread t2(findMaxRange, RANGE1, RANGE2, arr, &max);
	std::thread t3(findMaxRange, RANGE2, RANGE3, arr, &max);
	std::thread t4(findMaxRange, RANGE3, RANGE4, arr, &max);
	std::thread t5(findMaxRange, RANGE4, RANGE5, arr, &max);
	std::thread t6(findMaxRange, RANGE5, RANGE6, arr, &max);
	std::thread t7(findMaxRange, RANGE6, RANGE7, arr, &max);
	std::thread t8(findMaxRange, RANGE7, RANGE8, arr, &max);
	std::thread t9(findMaxRange, RANGE8, RANGE9, arr, &max);
	std::thread t10(findMaxRange, RANGE9, RANGE10, arr, &max);

	t1.join();//closing all the threads
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();
	t10.join();
	
	return max;
}

/*
 * function inserts to max the biggest value in this range if it is bigger
 * param start - the start of the range
 * param end - the end of the range
 * param arr - the array
 * param max - pointer to the max number so far
 */
void findMaxRange(int start, int end, int* arr, int* max)
{
	int curr = 0;

	for (int i = start; i <= end; i+=RANGE1)
	{
		curr = arr[i];
		if (curr > *max)//getting the max num in the range
		{
			*max = curr;
		}
	}
}

/*
 * recursive function; prints a message than creating a thread that calls the func
 * param num - the index of the thread(1 - 50)
 * return - none
 */
void threadToThread(int num)
{
	if (num > NUM_OF_THREADS)//exiting func
	{
		return;
	}
	std::cout << "Hello from Thread " << num << std::endl;//printing msg
	std::thread t(threadToThread, num + 1);//creating a thread, calling the func

	t.join();//closing the thread
}

/*
 *fucntion checks if a number is primary
 *param num - the number to check
 *return - whether the number is primary or not
 */
bool isPrime(int num) {
    bool flag = true;

	if(!num || num == 1)//0 and 1 are not exactly primary
	{
		return false;
	}


	for (int i = PRIME_NUM; i <= num / PRIME_NUM; i++) {
        if (num % i == 0) {
            flag = false;
            break;
        }
    }
    return flag;
}

