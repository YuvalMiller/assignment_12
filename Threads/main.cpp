#include "threads.h"

#define START_RANGE1 58
#define END_RANGE1 100
#define START_RANGE2 93
#define END_RANGE2 289
#define RANGE3 1000
#define RANGE4 100000
#define RANGE5 1000000


int main()
{
	int max = findMax();

	call_I_Love_Threads();
	std::cout << std::endl << "part 1 done" << std::endl << std::endl;

	std::vector<int> primes1;
	getPrimes(START_RANGE1, END_RANGE1, primes1);

	std::vector<int> primes3 = callGetPrimes(START_RANGE2, END_RANGE2);
	primes3 = callGetPrimes(0, RANGE3);
	primes3 = callGetPrimes(0, RANGE4);
	primes3 = callGetPrimes(0, RANGE5);
	std::cout << std::endl << "part 3 done" << std::endl << std::endl;

	callWritePrimesMultipleThreads(0, RANGE3, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, RANGE4, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, RANGE5, "primes2.txt", 2);
	std::cout << std::endl << "part 5 done" << std::endl;

	callPrintColor();
	std::cout << std::endl << "color bonus done" << std::endl << std::endl;
	
	std::cout << max << std::endl;
	std::cout << std::endl << "Max bonus done" << std::endl << std::endl;

	threadToThread(1);
	std::cout << std::endl << "Thread creating a thread in itself bonus done" << std::endl << std::endl;

	return 0;
}