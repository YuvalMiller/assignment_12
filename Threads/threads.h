#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <chrono>
#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <mutex>

void I_Love_Threads();//part 1
void call_I_Love_Threads();

void printVector(std::vector<int> primes);//part 2
bool isPrime(int num);

void getPrimes(int begin, int end, std::vector<int>& primes);//part 2
std::vector<int> callGetPrimes(int begin, int end);

void writePrimesToFile(int begin, int end, std::ofstream& file);//part 4
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

void printColor(const char* color);//bonus 1
void callPrintColor();

int findMax();//bonus 2
void findMaxRange(int start, int end, int* arr, int* max);

void threadToThread(int num);