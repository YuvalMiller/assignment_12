#include "MessageSender.h"

int main() {
	MessageSender ms;
	std::thread b(&MessageSender::readToQueue, &ms);
	std::thread c(&MessageSender::sendMessages, &ms);

	b.join();
	c.join();

	std::cout << "Both threads finished\n";
	return 0;
}