#pragma once
#include "pch.h"

class MessageSender {
public:
	MessageSender();//d'tor, c'tor
	~MessageSender();
	void menu();//menu func
	void  readToQueue();
	void sendMessages();
private:
	//fields
	std::vector<std::string> _users;
	std::queue<std::string> _messages;

	//methods
	void signIn();
	void signOut();
	void connectedUsers();

	bool doesExist(std::string val);//auxilery funcs
	bool isEmpty(std::ifstream& pFile);
};