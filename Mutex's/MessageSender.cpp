#include "MessageSender.h"

#define IN 1
#define OUT 2
#define CONN 3
#define EXIT 4
#define SLEEP_TIME 60

std::mutex mu;
std::condition_variable cond;

MessageSender::MessageSender()
{ //c'tor
	menu();
}

MessageSender::~MessageSender() = default; //default d'tor

/*
* its a menu.
*/
void MessageSender::menu()
{
	int foo = 0;

	while (foo != EXIT) {
		std::cout << "Welcome to MessageSender!" << std::endl;//printing the menu
		std::cout << "1. Sign in" << std::endl;
		std::cout << "2. Sign out" << std::endl;
		std::cout << "3. Show connected users" << std::endl;
		std::cout << "4. Exit" << std::endl;

		std::cin >> foo;
		std::cout << std::endl;

		switch (foo) {//the options
		case IN:
			signIn();
			break;

		case OUT:
			signOut();
			break;

		case CONN:
			connectedUsers();
			break;
		}
	}
}

/*
* function signs in a user if doesn't exist in the db
* return - none
*/
void MessageSender::signIn()
{
	std::string name;
	
	std::cout << "Enter username: ";//scanning username
	std::cin >> name;
	std::cout << std::endl;

	if (doesExist(name)) {//checking if exists, returning if not
		std::cout << "User already in db" << std::endl;
		return;
	}

	_users.push_back(name);//adding the name to the db
}

/*
* function signs out a user from the db
* return - none
*/
void MessageSender::signOut()
{
	std::string name;

	std::cout << "Enter username: ";//scanning username
	std::cin >> name;
	std::cout << std::endl;

	if (!doesExist(name)) {//checking if exists, exiting if does
		std::cout << "User is not in db" << std::endl;
		return;
	}

	for (std::vector<std::string>::iterator it = _users.begin(); it != _users.end(); it++)
	{
		if (*it == name) {//removing the name from the db
			_users.erase(it);
			return;
		}
	}
}

/*
* function prints all the connected users
* return - none
*/
void MessageSender::connectedUsers()
{
	for (int i = 0; i < _users.size(); i++)//going over the vector and printing each name
	{
		std::cout << i + 1 << ". " << _users[i] << std::endl;
	}

	std::cout << std::endl;
}

/*
* function checks if a name exists in the db
* param val - the name to check
* return - if exists
*/
bool MessageSender::doesExist(std::string val)
{
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i] == val) {//checking if exists by going over the vector
			return true;
		}
	}
	return false;
}

/*
* function reads a file to a queue, empties the file and waits 60 seconds
* return - none
*/
void MessageSender::readToQueue()
{
	std::ifstream file("data.txt");//openning the file
	std::string line;

	std::cout << "Entered thread1\n";

	do {
		std::unique_lock<std::mutex> msgLocker(mu);//locking the mutex so messages won't be changed by both funcs

		while (std::getline(file, line)) {//reading to the queue
			_messages.push(line);
		}

		msgLocker.unlock();	
		file.close();

		file.open("data.txt", std::ofstream::out | std::ofstream::trunc);//emptying the file
		file.close();

		std::cout << "Thread1 ended\n";
		cond.notify_all(); //notifying the waiting thread
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_TIME));//waiting for 60 seconds
	} while (!isEmpty(file));
}

/*
* function "sends messages" and practically writes the users and the messages to output.txt
* return - none
*/
void MessageSender::sendMessages()
{
	std::cout << "Entered thread2\n";
	std::cout << "Waiting for thread1 to finish\n";

	std::queue<std::string> temp = _messages;//to save the messages before popping everything
	std::ofstream file;//opnning the file
	file.open("output.txt");

	if (!file.is_open()) {
		std::cout << "file didn't open" << std::endl;
		return;
	}

	std::unique_lock<std::mutex> msgLocker(mu);//locking the mutex so messages won't be changed by both funcs
	cond.wait(msgLocker);
	std::cout << "Thread2 started writing\n";

	for (int i = 0; i < _messages.size(); i++)
	{
		for (int j = 0; j < _users.size(); j++)
		{
			file << _users[j] << " :" << _messages.front() << "\n";//writing to the file
			_messages.pop();
		}
	}
	msgLocker.unlock();
	std::cout << "Thread2 done writing\n";
	_messages = temp;//restoring messages
	file.close();//closing file
}
	
/*
* function checks if file is empty
* param pFile - the file
* return - if the file is empty
*/
bool MessageSender::isEmpty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}